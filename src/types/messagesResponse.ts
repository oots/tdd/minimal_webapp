export type MessagesResponse = {
  header: string;
  body: string;
};
