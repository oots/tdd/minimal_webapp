export type VehiclesData = {
  _declaration: Declaration;
  'query:QueryResponse': QueryQueryResponse;
};

export type Declaration = {
  _attributes: DeclarationAttributes;
};

export type DeclarationAttributes = {
  version: string;
  encoding: string;
};

export type QueryQueryResponse = {
  _attributes: QueryQueryResponseAttributes;
  'rim:Slot': RimSlotElement[];
  'rim:VatResponse': RimVatResponse;
  'rim:RegistryObjectList': RimRegistryObjectList[];
  'rim:AviResponse': RimAVIResponse[];
};

export type QueryQueryResponseAttributes = {
  requestId: string;
};

export type RimAVIResponse = {
  header: Header;
  body: RimAVIResponseBody;
};

export type RimAVIResponseBody = {
  VehicleCountries: VehicleCountries;
};

export type VehicleCountries = {
  VehicleCountry: VehicleCountry;
};

export type VehicleCountry = {
  VehReplyingCountryCode: MessageID;
  VehBodyReplies: VehBodyReplies;
};

export type VehBodyReplies = {
  VehBodyReply: VehBodyReply;
};

export type VehBodyReply = {
  VehicleRegistrationData: VehicleRegistrationData;
  VehicleTechnicalData: VehicleTechnicalData;
};

export type VehicleRegistrationData = {
  VehRegistrationNumber: VehRegistrationNumber;
  VehRegistrationDocDetails: VehRegistrationDocDetails;
  VehFirstRegistrationDateWorld: MessageID;
  VehStartRegistrationDate: MessageID;
  VehRoadworthiness: VehRoadworthiness;
};

export type MessageID = {
  _text: string;
};

export type VehRegistrationDocDetails = {
  VehRegistrationDocDetail: VehRegistrationDocDetail;
};

export type VehRegistrationDocDetail = {
  VehDocumentID: MessageID;
  VehDocIdIssuingAuthority: MessageID;
};

export type VehRegistrationNumber = {
  VehRegistrationNumberPart1: MessageID;
};

export type VehRoadworthiness = {
  VehRoadworthinessTestEndDate: MessageID;
};

export type VehicleTechnicalData = {
  VehicleIdentificationNumber: MessageID;
  Type: MessageID;
  Variant: MessageID;
  Version: MessageID;
  VehicleCategoryCode: MessageID;
  TypeApprovalNumber: MessageID;
  NumberOfAxles: MessageID;
  Length: MessageID;
  Width: MessageID;
  Height: MessageID;
  ActualMassOfTheVehicle: MessageID;
  PrimaryColourCode: MessageID;
  NrOfSeatingPositions: MessageID;
  PureElectricVehIndicator: MessageID;
  HybridVehIndicator: MessageID;
  MakeTable: MakeTable;
  CommercialNameTable: CommercialNameTable;
  AxleTable: AxleTable;
  WLTPEmissionTestParamGroup: WLTPEmissionTestParamGroup;
  EngineTable: EngineTable;
  InServiceMaxMassNatTable: InServiceMaxMassNatTable;
  NumberOfAxlesWithTwinWheels?: MessageID;
  Wheelbase?: MessageID;
  WheelbaseMaximum?: MessageID;
  TechnPermMaxMassCombination?: MessageID;
  AdvancedBrakingSystemCode?: MessageID;
  BodyworkTable?: BodyworkTable;
};

export type AxleTable = {
  AxleGroup: AxleGroup[];
};

export type AxleGroup = {
  AxleNumber: MessageID;
  TechnicallyPermMassAxle: MessageID;
  TyreAxleTable: TyreAxleTable;
  TwinWheelsAxleInd?: MessageID;
  AxleWithAirSuspOrEquivInd?: MessageID;
};

export type TyreAxleTable = {
  TyreAxleGroup: TyreAxleGroup;
};

export type TyreAxleGroup = {
  TyreNumber: MessageID;
  TyreSize?: MessageID;
  LoadCapacityIndexSingleWheel?: MessageID;
  SpeedCategorySymbol?: MessageID;
  LoadCapacityIndexTwinWheel?: MessageID;
};

export type BodyworkTable = {
  BodyworkGroup: BodyworkGroup;
};

export type BodyworkGroup = {
  CodeForBodywork: MessageID;
};

export type CommercialNameTable = {
  CommercialNameGroup: CommercialNameGroup;
};

export type CommercialNameGroup = {
  CommercialName: MessageID;
};

export type EngineTable = {
  EngineGroup: EngineGroup;
};

export type EngineGroup = {
  EngineCodeAsMarkedOnTheEngine: MessageID;
  NumberOfCylinders: MessageID;
  EngineCapacity: MessageID;
  ElectricEngineIndicator: MessageID;
  LpgFuellingSystemIndicator: MessageID;
  CngFuellingSystemIndicator: MessageID;
  FuelTable: FuelTable;
};

export type FuelTable = {
  FuelGroup: FuelGroup;
};

export type FuelGroup = {
  FuelCode: MessageID;
  MaximumNetPower: MessageID;
};

export type InServiceMaxMassNatTable = {
  InServiceMaxMassNatGroup: InServiceMaxMassNatGroup;
};

export type InServiceMaxMassNatGroup = {
  MaxPermMassNatTraffCountryCode: MessageID;
  MaxPermLadenMassNational: MessageID;
};

export type MakeTable = {
  MakeGroup: MakeGroup;
};

export type MakeGroup = {
  Make: MessageID;
};

export type WLTPEmissionTestParamGroup = {};

export type Header = {
  MessageID: MessageID;
  MessageRefID: MessageID;
  MessageVersion: MessageID;
  ServiceExecutionReason: ServiceExecutionReason;
  RecipientCountry: MessageID;
  SenderCountry: MessageID;
  SenderOrganisation: SenderOrganisation;
  SenderName: MessageID;
  SenderOrganisationName?: MessageID;
  TimeStamp: MessageID;
  TimeOut: MessageID;
};

export type SenderOrganisation = {
  SenderOrganisationCode: MessageID;
  SenderOrganisationDesc: MessageID;
};

export type ServiceExecutionReason = {
  ServiceExecutionReasonCode: MessageID;
  ServiceExecutionReasonDesc: MessageID;
};

export type RimRegistryObjectList = {
  'rim:RegistryObject': RimRegistryObject;
};

export type RimRegistryObject = {
  'rim:Slot': RimRegistryObjectRimSlot;
};

export type RimRegistryObjectRimSlot = {
  _attributes: RimSlotAttributes;
  'rim:SlotValue': PurpleRimSlotValue;
};

export type RimSlotAttributes = {
  name: string;
};

export type PurpleRimSlotValue = {
  _attributes: PurpleAttributes;
  'rim:Slot': RimSlotValueRimSlot;
  'rim:RepositoryItemRef': RimRepositoryItemRef;
};

export type PurpleAttributes = {
  'xsi:type': string;
  id: string;
};

export type RimRepositoryItemRef = {
  _attributes: RimRepositoryItemRefAttributes;
};

export type RimRepositoryItemRefAttributes = {
  'xlink:href': string;
  'xlink:title': string;
};

export type RimSlotValueRimSlot = {
  _attributes: RimSlotAttributes;
  'rim:SlotValue': FluffyRimSlotValue;
};

export type FluffyRimSlotValue = {
  _attributes: RimElementAttributes;
  'sdg:Evidence': SdgEvidence;
};

export type RimElementAttributes = {
  'xsi:type': string;
};

export type SdgEvidence = {
  Identifier: MessageID;
  IsAbout: IsAbout;
  IssuingAuthority: IssuingAuthority;
  IsConformantTo: IsConformantTo;
  IssuingDate: MessageID;
  Distribution: Distribution;
  ValidityPeriod: ValidityPeriod;
};

export type Distribution = {
  Format: MessageID;
};

export type IsAbout = {
  NaturalPerson: NaturalPerson;
};

export type NaturalPerson = {
  Identifier: Identifier;
  FamilyName: MessageID;
  GivenName: MessageID;
  DateOfBirth: MessageID;
};

export type Identifier = {
  _attributes: SdgIdentifierAttributes;
  _text: string;
};

export type SdgIdentifierAttributes = {
  schemeID: string;
};

export type IsConformantTo = {
  EvidenceTypeClassification: MessageID;
  Title: MessageID;
  Description: MessageID;
};

export type IssuingAuthority = {
  Identifier: Identifier;
  Name: Name;
};

export type Name = {
  _attributes: SdgNameAttributes;
  _text: string;
};

export type SdgNameAttributes = {
  lang: string;
};

export type ValidityPeriod = {
  StartDate: MessageID;
  EndDate: MessageID;
};

export type RimSlotElement = {
  _attributes: RimSlotAttributes;
  'rim:SlotValue': TentacledRimSlotValue;
};

export type TentacledRimSlotValue = {
  _attributes: RimElementAttributes;
  'rim:Value'?: MessageID;
  'rim:Element'?: RimElement;
  'sdg:Agent'?: RimSlotValueSdgAgent;
};

export type RimElement = {
  _attributes: RimElementAttributes;
  'sdg:Agent': RimElementSdgAgent;
};

export type RimElementSdgAgent = {
  'sdg:Identifier': Identifier;
  'sdg:Name': Name;
  'sdg:Address': SdgAddress;
  'sdg:Classification': MessageID;
};

export type SdgAddress = {
  'sdg:FullAddress': MessageID;
  'sdg:LocatorDesignator': MessageID;
  'sdg:PostCode': MessageID;
  'sdg:PostCityName': MessageID;
  'sdg:AdminUnitLevel1': MessageID;
  'sdg:AdminUnitLevel2': MessageID;
};

export type RimSlotValueSdgAgent = {
  'sdg:Identifier': Identifier;
  'sdg:Name': MessageID;
};

export type RimVatResponse = {
  header: Header;
  body: RimVatResponseBody;
};

export type RimVatResponseBody = {
  InformationResponse: InformationResponse;
};

export type InformationResponse = {
  RequestId: MessageID;
  VehHeldOwnedReferencedateTime: MessageID;
  VehiclesReply: VehiclesReply;
};

export type VehiclesReply = {
  ListOfVehiclesHeldAndOwned: ListOfVehiclesHeldAndOwned[];
};

export type ListOfVehiclesHeldAndOwned = {
  VehicleIdentificationNumber: MessageID;
  VehRegistrationNumber: VehRegistrationNumber;
  VehicleIsHeld: MessageID;
  VehicleIsOwned: MessageID;
};
