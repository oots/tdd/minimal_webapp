import { RimAVIResponse, VehicleTechnicalData } from '@/types/vehiclesData';
import clsx from 'clsx';
import { FaCheckSquare } from 'react-icons/fa';
import { ImCross } from 'react-icons/im';
import { xml2json } from 'xml-js';
import styles from './styles.module.scss';

type TableProps = {
  data: any;
};

const Table = ({ data }: TableProps) => {
  const apiData = JSON.parse(data.body);

  const vehiclesData = JSON.parse(
    xml2json(
      Buffer.from(apiData.payload[0].value, 'base64').toString('utf-8'),
      {
        compact: true,
        spaces: 4,
      }
    )
  );

  const vehiclesReply =
    vehiclesData['query:QueryResponse']['rim:VatResponse'].body
      .InformationResponse.VehiclesReply;

  console.log(vehiclesData['query:QueryResponse']['rim:VatResponse']);

  if (
    vehiclesData['query:QueryResponse']['rim:VatResponse'].body
      .InformationResponse.InformationResponseMessages &&
    vehiclesData['query:QueryResponse']['rim:VatResponse'].body
      .InformationResponse.InformationResponseMessages
      .InformationResponseMessage.InformationResponseMessageDesc._text ===
      'No information found'
  ) {
    return (
      <>
        <div className={clsx('w-2/12 rounded-lg bg-white py-2', styles.table)}>
          Either the user does not have associated vehicles or is not registered
          in EUCARIS
        </div>
      </>
    );
  }

  // This is "needed" because if there is only 1 car, the response it not an array and we need it to be an array
  if (vehiclesReply && !vehiclesReply.ListOfVehiclesHeldAndOwned[0]) {
    vehiclesReply.ListOfVehiclesHeldAndOwned = [
      vehiclesReply.ListOfVehiclesHeldAndOwned,
    ];
  }

  if (!vehiclesData['query:QueryResponse']['rim:AviResponse'][0]) {
    vehiclesData['query:QueryResponse']['rim:AviResponse'] = [
      vehiclesData['query:QueryResponse']['rim:AviResponse'],
    ];
  }

  return (
    <>
      {vehiclesData && (
        <table className={clsx('rounded-lg bg-white', styles.table)}>
          <thead>
            <tr className="bg-slate-700 text-white">
              <th>Vehicle Identification Number</th>
              <th>Held</th>
              <th>Owned</th>
              <th>Type</th>
              <th>Vehicle Category Code</th>
              <th>Version</th>
              <th>Brand</th>
            </tr>
          </thead>
          <tbody>
            {vehiclesData['query:QueryResponse']['rim:AviResponse'].map(
              (vehicle: RimAVIResponse, index: number) => {
                const vehicleTechnicalData: VehicleTechnicalData =
                  vehicle.body.VehicleCountries.VehicleCountry.VehBodyReplies
                    .VehBodyReply.VehicleTechnicalData;

                return (
                  <>
                    <tr key={index}>
                      <td key={index}>
                        {
                          vehiclesReply.ListOfVehiclesHeldAndOwned[index]
                            .VehicleIdentificationNumber._text
                        }
                      </td>
                      <td className={styles.check}>
                        <div className="flex justify-center">
                          {vehiclesReply.ListOfVehiclesHeldAndOwned[index]
                            .VehicleIsHeld._text === 'true' ? (
                            <FaCheckSquare />
                          ) : (
                            <ImCross />
                          )}
                        </div>
                      </td>
                      <td className={styles.check}>
                        <div className="flex justify-center">
                          {vehiclesReply.ListOfVehiclesHeldAndOwned[index] &&
                          vehiclesReply.ListOfVehiclesHeldAndOwned[index]
                            .VehicleIsOwned._text === 'true' ? (
                            <FaCheckSquare />
                          ) : (
                            <ImCross />
                          )}
                        </div>
                      </td>
                      <td>{vehicleTechnicalData.Type._text}</td>
                      <td>{vehicleTechnicalData.VehicleCategoryCode._text}</td>
                      <td>{vehicleTechnicalData.Version._text}</td>
                      <td>
                        {vehicleTechnicalData.MakeTable.MakeGroup.Make._text}
                      </td>
                    </tr>
                  </>
                );
              }
            )}
          </tbody>
        </table>
      )}
    </>
  );
};

export default Table;
