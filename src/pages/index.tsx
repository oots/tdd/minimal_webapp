import { MessagesResponse } from '@/types/messagesResponse';
import clsx from 'clsx';
import Head from 'next/head';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { MoonLoader } from 'react-spinners';
import useSWR, { Fetcher, Key } from 'swr';
import Button from '../components/Button';
import Table from '../components/Table';
import styles from '../styles/Home.module.scss';

const fetcher: Fetcher<MessagesResponse, Key> = (
  input: RequestInfo,
  init?: RequestInit
) => fetch(input, init).then((res) => res.json());

export default function Home() {
  const [state, setState] = useState(0);
  const [messageId, setMessageId] = useState('');
  const [apiData, setApiData] = useState<MessagesResponse>();
  const [person, setPerson] = useState(0); //type of person (Natural or Legal)

  /*
    - 0: Start state
    - 1: Loading state
    - 2: Final state
  */

  const backendUrl1 = `${process.env.NEXT_PUBLIC_OOTS_API}/getMessageById/${messageId}`;
  const backendUrl2 = `${process.env.NEXT_PUBLIC_OOTS_API}/submitMessage`;

  const { data, error } = useSWR<MessagesResponse, unknown>(
    messageId ? [backendUrl1] : null,
    fetcher,
    {
      refreshInterval: 10010,
    }
  );

  useEffect(() => {
    if (data?.header && !apiData) {
      setApiData(data);
    }
    if (apiData && state === 1) {
      setState(2);
    }
  }, [apiData, data, state]);

  const resetForm = () => {
    setState(0);
    setMessageId('');
    setApiData(undefined);
  };

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data: any) => {
    setState(1);
    reset();

    if (person === 0) {
      data.typeOfPerson = 0;
    } else {
      data.typeOfPerson = 1;
      data.surName = '';
      data.dateOfBirth = '';
    }

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ data }),
    };

    const res = await fetch(backendUrl2, requestOptions);
    const resData = await res.json();
    setMessageId(resData.messageId);
  };

  const toggleForm = (personN: number) => {
    if (person !== personN) {
      reset();
      setPerson(personN);
    }
  };

  if (error) {
    return (
      <div>
        <div>Something went wrong</div>
        <div>{`Error: ${error}`}</div>
      </div>
    );
  }

  return (
    <>
      <Head>
        <title>Minimal Demo App</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main
        className={clsx(
          styles.container,
          'flex h-full w-full flex-col items-center justify-center pb-40'
        )}
      >
        <h1 className="mb-16 text-5xl">OOTS Eucaris Bridge App</h1>
        {state === 0 && (
          <form
            className={clsx(styles.form, 'flex flex-col rounded-lg bg-white')}
            onSubmit={handleSubmit(onSubmit)}
          >
            <div className="flex items-center justify-between rounded-t-lg bg-slate-700 px-7 py-4 text-xl text-white">
              <div>Insert your data</div>
              <div
                className={clsx(
                  styles.toggleButton,
                  'flex flex-row rounded-lg p-1'
                )}
              >
                <div
                  className={
                    person === 0
                      ? clsx(styles.button1, 'rounded-lg bg-slate-700 p-1')
                      : clsx(
                          styles.button2,
                          'cursor-pointer rounded-lg bg-white p-1 text-black'
                        )
                  }
                  onClick={() => toggleForm(0)}
                >
                  Natural Person
                </div>
                <div
                  className={
                    person === 1
                      ? clsx(styles.button1, 'rounded-lg bg-slate-700 p-1')
                      : clsx(
                          styles.button2,
                          'cursor-pointer rounded-lg bg-white p-1 text-black'
                        )
                  }
                  onClick={() => toggleForm(1)}
                >
                  Legal Person
                </div>
              </div>
            </div>
            <div className={styles.inputFields}>
              {person === 0 && (
                <>
                  <div
                    className={clsx(
                      styles.inputField,
                      'flex flex-col px-10 py-2'
                    )}
                  >
                    <label>Name</label>
                    <input
                      type="text"
                      className="mt-1 h-8 rounded-lg bg-slate-200 pl-3"
                      {...register('name', { required: true })}
                    />
                  </div>
                  <div
                    className={clsx(
                      styles.inputField,
                      'flex flex-col px-10 py-2'
                    )}
                  >
                    <label>Surname</label>
                    <input
                      type="text"
                      className="mt-1 h-8 rounded-lg bg-slate-200 pl-3"
                      {...register('surName', { required: true })}
                    />
                  </div>
                  <div
                    className={clsx(
                      styles.inputField,
                      'flex flex-col px-10 py-2 pb-10'
                    )}
                  >
                    <label>Date Of Birth</label>
                    <input
                      type="date"
                      placeholder="dd-mm-yyyy"
                      className="mt-1 h-8 rounded-lg bg-slate-200 px-2"
                      {...register('dateOfBirth', { required: true })}
                    />
                  </div>
                </>
              )}
              {person === 1 && (
                <>
                  <div
                    className={clsx(
                      styles.inputField,
                      'flex flex-col px-10 py-2 pb-10'
                    )}
                  >
                    <label>Name</label>
                    <input
                      type="text"
                      className="mt-1 h-8 rounded-lg bg-slate-200 pl-3"
                      {...register('name', { required: true })}
                    />
                  </div>
                </>
              )}
            </div>
            <div className="flex items-center justify-center rounded-lg">
              <input
                type="submit"
                value="Check results"
                className="mb-8 cursor-pointer rounded-lg bg-green-300 p-2 px-4"
              />
            </div>
          </form>
        )}
        {state === 1 && (
          <>
            <div>
              <MoonLoader color="#000" />
            </div>
          </>
        )}
        {state === 2 && (
          <>
            <Table data={data} />
            <div className="mt-10">
              <Button
                variant="primary"
                className="bg-green-300"
                onClick={resetForm}
              >
                Restart request
              </Button>
            </div>
          </>
        )}
      </main>
    </>
  );
}
